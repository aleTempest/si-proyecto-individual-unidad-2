from PyQt6.QtWidgets import (QMainWindow,
        QFileDialog, QApplication, QDialog, QLabel, QVBoxLayout, QHBoxLayout,
        QGridLayout, QListWidget, QWidget, QListWidgetItem, QScrollArea)
from PyQt6.QtGui import QIcon, QAction, QPixmap, QColor, QPalette
from pathlib import Path
import sys, os
from PyQt6 import QtCore
from PyQt6.QtCore import Qt, QSize
class DirItem():


    def __init__(self):
        self.name = ""
        self.fullPath = ""

    def setName(self, name: str):
        self.name = name
        return self

    def setFullPath(self, fullPath: str):
        self.fullPath = fullPath
        return self

    def __str__(self) -> str:
        return self.name


class MainWindow(QMainWindow):


    def __init__(self):
        super().__init__()
        self.initUI()


    def initUI(self):
        self.statusBar()

        # Acción para el item del menu
        openFile = QAction('Selecc. Dataset', self)
        openFile.setShortcut('Ctrl+O')
        openFile.setStatusTip('Seleccionar carpeta')

        # señal para asignar la función `showDialog` al item del menu
        openFile.triggered.connect(self.showDialog)

        showAuthors = QAction('Acerca de', self)
        showAuthors.setShortcut('Ctrl+a')
        showAuthors.setStatusTip('Mostrar autores')

        showAuthors.triggered.connect(self.showAuthorsDialog)

        # Menú principal
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&Abrir')
        fileMenu.addAction(openFile)
        fileMenu.addAction(showAuthors)

        # Main
        # TODO implementar paginado o alguna manera de seguir viendo más 
        # imágenes después del límite
        self.mainLayout = QHBoxLayout()
        self.scroll = QScrollArea()
        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOn)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.scroll.setWidgetResizable(True)

        # Layout para mostrar la lista de clases del dataset
        self.sideLayout = QVBoxLayout()

        # Layout para mostrar las imágenes del dataset
        self.gridLayout = QGridLayout()

        self.imageContainer = QWidget()
        self.imageContainer.setLayout(self.gridLayout)
        self.scroll.setWidget(self.imageContainer)


        # TODO layout para "evaluación" de los clasificadores

        # Añadir layouts a la principal
        # self.scroll.setLayout(self.gridLayout)
        self.mainLayout.addLayout(self.sideLayout)
        self.mainLayout.addWidget(self.scroll)
        # self.mainLayout.addLayout(self.gridLayout)

        # Crear un widget central para la layout principal
        centralWidget = QWidget(self)
        centralWidget.setLayout(self.mainLayout)
        self.setCentralWidget(centralWidget)

        self.setGeometry(300, 300, 550, 450)
        self.setWindowTitle('Proyecto Individial')
        self.show()

        
    def loadPicture(self, path: str):
        img = QPixmap(path)
        img = img.scaled(128, 128, QtCore.Qt.AspectRatioMode.KeepAspectRatio)
        label = QLabel()
        label.setPixmap(img)
        return label



    def customDialog(self, dialogTitle: str, dialogText: str):
        dialog = QDialog(self)
        dialog.setWindowTitle(dialogTitle)
        
        label = QLabel(dialogText)

        dialog_layout = QVBoxLayout()
        dialog_layout.addWidget(label)
        dialog.setLayout(dialog_layout)
        dialog.show()


    def showAuthorsDialog(self):
        authors = """
        Agustín Alejandro Mota Hinojosa
        Ramón Uriel Hernadez Sanchez
        Dafne Patricia Moreno Flores
        Alejandra Carolina Rodriguez Porras
        """

        self.customDialog('Autores',authors)


    def scandir(self, dirname: str):
        """
        Escanea un directorio dado por argumento de manera recursiva y 
        retorna una lista con la ruta absoluta de cada subdirectorio

        Args:
            dirname: Directorio que será escaneado

        Returns:
            list: Lista de strings con subdirectorios
        """

        subfolders = [f.path for f in os.scandir(dirname) if f.is_dir()]
        for dirname in list(subfolders):
            subfolders.extend(self.scandir(dirname))
        return subfolders

    def getPictures(self, dirname: str):
        return [f.path for f in os.scandir(dirname) if self.isPicture(f.path)]


    def isPicture(self, filename: str):
        """
        Verificar si un archivo es una imagen

        Args:
            filename: Nombre del archivo

        Returns:
            Boolean
        """
        FILE_EXTS = ['png', 'jpg', 'jpeg', 'avif']
        return filename if self.splitString(filename, '.') in FILE_EXTS else ""


    def splitString(self, _str: str, char: str):
        """
        Toma una string se deshace de todos los cárácteres antes del carácter
        pasado por argumento

        Params:
            _str: documentar
            char: documentar
        Returns:
            String recortada
        """
        return _str[_str.rfind(char) + 1:len(_str) + 1]


    def getDirNames(self, dirList: list):
        """
        toma una lista de strings que contiene directorios como '/etc/sys' y
        toma la última palabra como el "nombre" (a partir del "/"), y se 
        deshace de todo lo demás.

        Params:
            dirList: Lista de directorios con "full path"

        Returns:
            list: Lista de nombre de directorios 
        """
        _dirList = []
        for dir in dirList:
            _dirList.append(self.splitString(dir, '/')) 
        return _dirList
    
    def getDirBundle(self, dirname: str):
        bundle = []
        dirs = self.scandir(dirname)
        for dir in dirs:
            dirItem = DirItem()
            dirItem.setName(self.splitString(dir, '/')).setFullPath(dir)
            bundle.append(dirItem)
        return bundle


    def showDialog(self, debug = False):
        """
        Muestra un selector de archivos en el que solo se pueden seleccionar
        carpetas, usa el directorio `home` como directorio por defecto
        """

        home_dir = str(Path.home()) 
        try:
            # debug sirve para poner un directorio por defecto en vez de abrir 
            # el diálogo a cada rato
            dir = QFileDialog.getExistingDirectory(
                self, 'Seleccionar Dataset', home_dir
                ) if not debug else "/home/ale/Downloads/CitrusUAT_datasetA/CitrusUAT_dataset/"
            listWidget = QListWidget()
            # TODO implementar lógica para detectar si la carpeta seleccionada
            # es un dataset
            items = self.getDirBundle(dir)

            # datos extra para cada item dentro de la lista
            for item in items:
                listItem = QListWidgetItem(item.name)
                listItem.setData(1, item)
                listWidget.addItem(listItem)

            # lmao
            # listWidget.addItems(str(dir) for dir in self.getDirBundle(dir))
            listWidget.currentItemChanged.connect(self.item_changed)
            self.sideLayout.addWidget(listWidget)

        except FileNotFoundError:
            self.customDialog('Error',
                              'Directorio no válido')


    # debug para listWidget
    def item_changed(self, curr, prev):
        if curr:
            dir = (curr.data(1)).fullPath
            pics = self.getPictures(dir)
            for i in range(0,15):
                for j in range(0,3):
                    picWidget = self.loadPicture(pics[i])
                    if (picWidget is not None):
                        self.gridLayout.addWidget(picWidget, i, j)
                        print("Loaded: " ,pics[i])


def main():
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
